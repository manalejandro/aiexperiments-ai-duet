FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    pkg-config \
    libpng-dev \
    libjpeg8-dev \
    libfreetype6-dev \
    libblas-dev \
    liblapack-dev \
    libatlas-base-dev \
    libsndfile1-dev \
    libasound2-dev \
    libjack-dev \
    gfortran \
    ffmpeg \
    llvm-8 \
    python \
    python2.7 \
    python3 \
    python3-dev \
    python3-pip \
    python3-venv \
    nvidia-cuda-dev \
    curl \
    nodejs \
    npm && \
    apt clean

RUN pip3 install --upgrade pip

COPY ./server/requirements.txt /tmp/
RUN pip3 install -r /tmp/requirements.txt

COPY . /src/

WORKDIR /src/static/
RUN npm install && npm run build

WORKDIR /src/server/

EXPOSE 8080
ENTRYPOINT python3 server.py
